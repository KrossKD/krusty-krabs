<nav class="navbar navbar-expand-lg navbar-dark bg-transparent fixed-top">
    <!-- <a class="navbar-brand" href="#">Navbar</a> -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <div class="row">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Menu</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Blog
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li><li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Pages
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact</a>
                </li>
                
                
            </ul>
        </div>
        <div class="row mx-auto">
            <a class="nav-link" href="#">
                <img class="img-fluid" src="..." alt="Logo aici">
            </a>
        </div>
        <a class="text-white mx-2" href="http://instagram.com"><i class="fa fa-instagram" aria-hidden="true"></i></a>
        <a class="text-white mx-2" href="http://twitter.com"><i class="fa fa-twitter" aria-hidden="true"></i></a>
        <a class="text-white mx-2" href="http://facebook.com"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a class="text-white mx-2" href="http://vk.com"><i class="fa fa-vk" aria-hidden="true"></i></a>
        <button type="button" class="btn btn-outline-warning rounded-pill mx-2">+373 786 466 23</button>
    </div>
</nav>
